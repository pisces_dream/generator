package com.generator.core.model;

import com.generator.core.util.DatabaseDataTypesUtils;
import com.generator.core.util.PropertiesUtil;

public class Column {

	private String columnName;
	private String typeName;
	private int dataType;
	private int columnSize;
	private int decimalDigits;
	private String remarks;
	private String javaType;

	public Column() {
	}

	public Column(String columnName, String typeName, int dataType, int columnSize, int decimalDigits, String remarks) {
		this.columnName = columnName;
		this.typeName = typeName;
		this.dataType = dataType;
		this.columnSize = columnSize;
		this.decimalDigits = decimalDigits;
		this.remarks = remarks;

		String normalJdbcJavaType = DatabaseDataTypesUtils.getPreferredJavaType(getDataType(), getColumnSize(), getDecimalDigits());
		this.javaType = PropertiesUtil.getInstance().getValue("java_typemapping." + normalJdbcJavaType, normalJdbcJavaType).trim();
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getColumnSize() {
		return columnSize;
	}

	public void setColumnSize(int columnSize) {
		this.columnSize = columnSize;
	}

	public int getDecimalDigits() {
		return decimalDigits;
	}

	public void setDecimalDigits(int decimalDigits) {
		this.decimalDigits = decimalDigits;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

}
