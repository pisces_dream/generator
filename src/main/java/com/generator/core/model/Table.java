package com.generator.core.model;

import java.util.ArrayList;
import java.util.List;

public class Table {

	private String tableName;
	private String tableType;

	private List<Column> columns = new ArrayList<Column>();

	public Table() {
	}

	public Table(String tableName, String tableType) {
		this.tableName = tableName;
		this.tableType = tableType;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public void addColumn(Column column) {
		this.columns.add(column);
	}

	public String getTableNameUpperCase() {
		char[] cs = tableName.toCharArray();
		cs[0] -= 32;
		return String.valueOf(cs);
	}

}
