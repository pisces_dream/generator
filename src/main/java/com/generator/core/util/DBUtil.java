package com.generator.core.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {
	private final static String DRIVER;
	private final static String URL;
	private final static String USER;
	private final static String PWD;

	static {
		PropertiesUtil prop = PropertiesUtil.getInstance();
		DRIVER = prop.getValue("jdbc.driver");
		URL = prop.getValue("jdbc.url");
		USER = prop.getValue("jdbc.username");
		PWD = prop.getValue("jdbc.password");

		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		Connection con = null;
		try {
			con = DriverManager.getConnection(URL, USER, PWD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;

	}

	public static void closeAll(Connection con, Statement stmt, ResultSet rs) {
		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			if (con != null)
				con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		System.out.println(getConnection());
	}

}
