package com.generator.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.generator.core.model.Table;

import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreeMarkerUtil {
	private static FreeMarkerUtil freeMarkerUtil = new FreeMarkerUtil();
	private static Configuration configuration = new Configuration();

	private void initConfig(File file) throws IOException {
		FileTemplateLoader[] templateLoaders = new FileTemplateLoader[] { new FileTemplateLoader(file) };
		MultiTemplateLoader multiTemplateLoader = new MultiTemplateLoader(templateLoaders);
		configuration.setTemplateLoader(multiTemplateLoader);
		configuration.setLocale(Locale.CHINA);
		configuration.setDefaultEncoding("utf-8");
		configuration.setEncoding(Locale.CHINA, "utf-8");
		configuration.setObjectWrapper(new DefaultObjectWrapper());
	}

	private void processByTable(Table table) throws UnsupportedEncodingException, FileNotFoundException {
		Map<String, Object> temp = new HashMap<String, Object>();
		temp.put("table", table);
		temp.put("className", table.getTableNameUpperCase());
		temp.put("basepackage", PropertiesUtil.getInstance().getValue("basepackage"));

		Writer out = new OutputStreamWriter(new FileOutputStream(PropertiesUtil.getInstance().getValue("outRoot") + "/" + table.getTableNameUpperCase() + ".java"), "UTF-8");
		freeMarkerUtil.processTemplate("${className}.java", temp, out);
	}

	private void processTemplate(String templateName, Map<?, ?> root, Writer out) {
		try {
			Template template = configuration.getTemplate(templateName);
			template.process(root, out);
			out.flush();
			System.out.println("执行完成......................................");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void generateByTable(String tableName, String templateRootDir) {
		try {
			File file = new File(templateRootDir).getAbsoluteFile();
			freeMarkerUtil.initConfig(file);

			MetaDataUtil metaData = new MetaDataUtil();
			Table table = metaData.getTables(tableName);

			freeMarkerUtil.processByTable(table);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void generateByAllTable(String templateRootDir) {
		try {
			File file = new File(templateRootDir).getAbsoluteFile();
			freeMarkerUtil.initConfig(file);

			MetaDataUtil metaData = new MetaDataUtil();
			List<Table> tables = metaData.getAllTables();

			for (Table table : tables) {
				freeMarkerUtil.processByTable(table);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
