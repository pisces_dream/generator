package com.generator.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
	private static PropertiesUtil instance;
	private Properties prop;

	private PropertiesUtil() {
		prop = new Properties();
		InputStream is = PropertiesUtil.class.getClassLoader().getResourceAsStream("config.properties");
		try {
			prop.load(is);
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized static PropertiesUtil getInstance() {
		if (instance == null) {
			instance = new PropertiesUtil();
		}
		return instance;
	}

	public String getValue(String key) {
		return this.prop.getProperty(key);
	}

	public String getValue(String key, String defaultValue) {
		String value = null;
		if (value == null || "".equals(value.trim())) {
			value = this.prop.getProperty(key);
		}
		return value == null || "".equals(value.trim()) ? defaultValue : value;
	}

	public static void main(String[] args) {
		System.out.println(PropertiesUtil.getInstance().getValue("jdbc.url"));
		String normalJdbcJavaType = "java.sql.String";
		System.out.println(PropertiesUtil.getInstance().getValue("java_typemapping." + normalJdbcJavaType, normalJdbcJavaType));
	}
}
